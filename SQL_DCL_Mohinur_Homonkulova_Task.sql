CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvd_rental TO rentaluser;

GRANT SELECT ON TABLE customer TO rentaluser;
SELECT * FROM customer;

CREATE GROUP rental;
GRANT rental TO rentaluser;

GRANT INSERT, UPDATE ON TABLE rental TO rental;
INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id) 
VALUES ('2024-03-24', 1, 1, '2024-03-28', 1);
UPDATE rental SET return_date = '2024-03-28' WHERE rental_id = 1;

REVOKE INSERT ON TABLE rental FROM rental;

CREATE ROLE client_Patricia_Johnson;
GRANT SELECT ON rental TO client_Patricia_Johnson;
GRANT SELECT ON payment TO client_Patricia_Johnson;

CREATE POLICY allowed_Patricia_Johnson_rental
ON rental
FOR SELECT
TO client_Patricia_Johnson
USING (customer_id = '2');
CREATE POLICY allowed_Patricia_Johnson_payment
ON payment
FOR SELECT
TO client_Patricia_Johnson
USING (customer_id = '2');

ALTER TABLE rental ENABLE ROW LEVEL SECURITY;
ALTER TABLE payment ENABLE ROW LEVEL SECURITY;

SET ROLE client_Patricia_Johnson;

SELECT * FROM rental;
SELECT * FROM payment;
RESET ROLE;